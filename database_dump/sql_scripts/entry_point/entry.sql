CREATE TABLE IF NOT EXISTS "ESTACAO_SOLAR_FGA" (
    "DATE" DATE,
    "TIME" TIME,
    "Direcao_Vento" FLOAT,
    "Pir_GHI_SN9045" FLOAT,
    "SR05_SN9045_GHI" FLOAT,
    "Temperatura_Ar" FLOAT,
    "Umidade_Ar" FLOAT,
    "Velocidade_Vento" FLOAT
);

COPY "ESTACAO_SOLAR_FGA" ("DATE", "TIME", "Direcao_Vento", "Pir_GHI_SN9045", "SR05_SN9045_GHI", "Temperatura_Ar", "Umidade_Ar", "Velocidade_Vento")  FROM '/docker-entrypoint-initdb.d/2021.csv';
COPY "ESTACAO_SOLAR_FGA" ("DATE", "TIME", "Direcao_Vento", "Pir_GHI_SN9045", "SR05_SN9045_GHI", "Temperatura_Ar", "Umidade_Ar", "Velocidade_Vento")  FROM '/docker-entrypoint-initdb.d/2022.csv';
COPY "ESTACAO_SOLAR_FGA" ("DATE", "TIME", "Direcao_Vento", "Pir_GHI_SN9045", "SR05_SN9045_GHI", "Temperatura_Ar", "Umidade_Ar", "Velocidade_Vento")  FROM '/docker-entrypoint-initdb.d/2023.csv';
COPY "ESTACAO_SOLAR_FGA" ("DATE", "TIME", "Direcao_Vento", "Pir_GHI_SN9045", "SR05_SN9045_GHI", "Temperatura_Ar", "Umidade_Ar", "Velocidade_Vento")  FROM '/docker-entrypoint-initdb.d/2024.csv';


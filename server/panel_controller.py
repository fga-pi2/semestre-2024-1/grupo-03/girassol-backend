import os
from pvlib.pvsystem import SingleAxisTrackerMount
from pvlib.location import Location
import datetime as dt


MAX_ANGLE = 25.6


class PanelController:
    def __init__(
        self, 
        latitude: float,
        longitude: float,
        tz: str='America/Sao_Paulo',
        current_tilt:float=0,
        max_angle: float=MAX_ANGLE,
        altitude=None, 
        last_position: str | None=None,
        **kwargs
    ):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.tz = tz
        self.tracker_mount = SingleAxisTrackerMount(
            axis_tilt=current_tilt,
            axis_azimuth=0,
            max_angle=max_angle,
            backtrack=False
        )
        self.default_positions = ['pos1', 'pos2', 'pos3', 'pos1']
        self.last_position = last_position
        self.position_to_tilt = {
            'pos1': 25.6,
            'pos2': 0.15,
            'pos3': -22.3,
        }
        self.kwargs = kwargs


    def get_default_times(self, day_now: dt.datetime):
        day_0_h  = day_now.replace(
            hour=0, minute=0, second=0, microsecond=0, tzinfo=dt.timezone.utc
        )

        return {
            'pos1': day_0_h + dt.timedelta(hours=14, minutes=4),
            'pos2': day_0_h + dt.timedelta(hours=14, minutes=5, seconds=10),
            'pos3': day_0_h + dt.timedelta(hours=18, minutes=5),
        } 


    def get_location(self) -> Location:
        return Location(
            tz=self.tz,
            name='Girassol',
            latitude=self.latitude, 
            longitude=self.longitude,
        )


    def get_orientation(self, dt: dt.datetime) -> float:
        location = self.get_location()
        sol_pos = location.get_solarposition([dt])
        return self.tracker_mount.get_orientation(
            solar_zenith=sol_pos['zenith'],
            solar_azimuth=sol_pos['azimuth'],
        )['tracker_theta'].tolist()[0]


    def get_default_position(self, dt_arg: dt.datetime) -> str:
        tilt = self.get_orientation(dt_arg)
        print('Current tracker tilt ', self.tracker_mount.axis_tilt)
        print(f'{dt_arg.isoformat()} Ideal orientation is: {tilt}')

        if os.environ.get('USE_SOLAR_LIB', 'false') == 'true':

            if abs(tilt - self.tracker_mount.axis_tilt) <= (MAX_ANGLE/2 + 3) and tilt > (MAX_ANGLE/2):
                print('Got pos1', tilt)
                self.last_position = self.default_positions[0]
            elif abs(tilt - self.tracker_mount.axis_tilt) > (MAX_ANGLE/2 + 3) and tilt >= 0.0:
                print('Got pos2', tilt)
                self.last_position = self.default_positions[1]
            elif abs(tilt - self.tracker_mount.axis_tilt) > (MAX_ANGLE/2 + 3) and tilt < 0.0:
                print('Got pos3', tilt)
                self.last_position = self.default_positions[2]
            elif dt_arg >= dt.datetime(
                year=dt_arg.year, 
                month=dt_arg.month,
                day=dt_arg.day,
                hour=dt.time(22).hour,
                tzinfo=dt.timezone.utc
            ):
                print('Got pos1', tilt)
                self.last_position = self.default_positions[3]
            elif self.last_position is None:
                print('None position', tilt)
                self.last_position = self.default_positions[0]

        else:
            if dt_arg <= self.get_default_times(dt_arg)['pos1']:
                print('Got pos1', dt_arg.isoformat())
                self.last_position = self.default_positions[0]
            elif dt_arg <= self.get_default_times(dt_arg)['pos2']:
                print('Got pos2', dt_arg.isoformat())
                self.last_position = self.default_positions[1]
            elif dt_arg <= self.get_default_times(dt_arg)['pos3']:
                print('Got pos3', dt_arg.isoformat())
                self.last_position = self.default_positions[2]
            elif dt_arg >= dt.datetime(
                year=dt_arg.year, 
                month=dt_arg.month,
                day=dt_arg.day,
                hour=dt.time(22).hour,
                tzinfo=dt.timezone.utc
            ):
                print('Got pos1', dt_arg.isoformat())
                self.last_position = self.default_positions[3]
            elif self.last_position is None:
                print('None position', dt_arg.isoformat())
                self.last_position = self.default_positions[0]

        self.tracker_mount.axis_tilt = self.position_to_tilt.get(self.last_position)
        print('Current position: ', self.last_position)

        return self.last_position


import os

MQTT_SERVICE_NAME:     str=os.environ.get('MQTT_SERVICE_NAME', 'mqtt-broker')
MQTT_SERVICE_PORT:     int=int(os.environ.get('MQTT_SERVICE_PORT', 1883))
POSTGRES_DB:           str = os.environ.get('POSTGRES_DB', 'girassoldatabase')
POSTGRES_USER:         str = os.environ.get('POSTGRES_USER', 'girassol')
POSTGRES_PASSWORD:     str = os.environ.get('POSTGRES_PASSWORD', 'girassol')
POSTGRES_HOST:         str = os.environ.get('POSTGRES_HOST', 'db')
POSTGRES_PORT:         int = int(os.environ.get('POSTGRES_PORT', 5432))
HISTORICAL_DATA_TABLE: str = os.environ.get('HISTORICAL_DATA_TABLE', 'ESTACAO_SOLAR_FGA')
EMAIL_CLIENT = os.environ.get("EMAIL_CLIENT", 'adriansoareslps@gmail.com')

from pandas import DataFrame
from pvlib.location import Location
import requests
from datetime import timezone, datetime, timedelta
from typing import Any, Dict
import os

from model import LatLong, get_db
from panel_controller import PanelController
# from sunposition import sunpos


DEFAULT_TIMEZONE = os.environ.get('DEFAULT_TIMEZONE', 'America/Sao_Paulo')
MAX_ANGLE = float(os.environ.get('MAX_ANGLE', 25.6))


class APIService:
    def __init__(self, lat: float, lon: float, locale: str):
        self.lat = lat
        self.lon = lon
        self.last_update_time: datetime | None = None
        self.get_now_time = lambda: datetime.now(timezone.utc)


class DefaultAPIService(APIService):
    def __init__(self, lat: float, lon: float, locale: str):
        super().__init__(lat, lon, locale)
        self.last_position = None
        self.panel_controller = PanelController(
            latitude=self.lat, 
            longitude=self.lon,
            tz=DEFAULT_TIMEZONE,
        )

    def get_data(self) -> Dict[str, Any]:
        location = Location(
            latitude=self.lat,
            longitude=self.lon,
            tz=DEFAULT_TIMEZONE,
            name='Girassol',
        )

        response = location.get_solarposition([
            self.get_now_time()
        ])

        az = response['azimuth']
        zn =  response['zenith']

        if type(response) == DataFrame:
            return { 
                'azimuth': az.iloc[0],
                'zenith': zn.iloc[0],
            }
        else:
            return {
                'azimuth': az[0],
                'zenith': zn[0]
            }


    def get_production(self) -> tuple[bool,Dict[str, Any]]:
        current_time = self.get_now_time()
        if self.last_update_time is not None and (current_time - self.last_update_time) < timedelta(hours=12):
            return (False, {})

        self.last_update_time = current_time

        base_url = "https://api.open-meteo.com/v1/forecast"
        params = {
            "latitude": self.lat,
            "longitude": self.lon,
            "hourly": "direct_radiation_instant",
            "timezone": "America/Sao_Paulo",
            "past_days": 16,
            "forecast_days": 16
        }

        response = requests.get(base_url, params=params)
        if response.status_code == 200:
            data: dict = response.json()
            print(f'METEO Response: {list(data.items())[:5]}')

            hourly_data = data.get('hourly', {}).get('time', [])
            hourly_radiation = data.get('hourly', {}).get(
                'direct_radiation_instant', [])

            if len(hourly_data) != len(hourly_radiation):
                print(f'[send_production] Failed to get production data corrupted hourly length')
                return (False, {})

            hourly_radiation_dict = {}
            for idx, time_str in enumerate(hourly_data):
                radiation_value = hourly_radiation[idx]
                hourly_radiation_dict[time_str] = radiation_value

            return (True, hourly_radiation_dict)

        else:
            print(f'[send_production] Failed to get production data response status: {response.status_code}')
            print(f'[send_production] {response.json()}')
            return (False, {})

    def get_wheather(self) -> tuple[bool,Dict[str, Any]]:
        base_url = "https://api.open-meteo.com/v1/forecast"
        params = {
            "latitude": self.lat,
            "longitude": self.lon,
            "current": "temperature_2m,relative_humidity_2m,apparent_temperature,is_day,precipitation,cloud_cover",
            "timezone": "America/Sao_Paulo",
            "past_days": 14,
            "forecast_days": 16
        }

        response = requests.get(base_url, params=params)
        if response.status_code == 200:
            data = response.json()
            return (True, data.get('current', {}))
        else:
            return (False, {})

    def get_lat_lon(self):
        db = next(get_db())
        with db.atomic():
            position = LatLong.select().limit(1).first()
            self.lat = position.lat
            self.lon = position.long

        return { 'lat': position.lat, 'lon': position.long }
        

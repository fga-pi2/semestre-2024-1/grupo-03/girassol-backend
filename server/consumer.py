import os
from peewee import DoesNotExist, fn
from sys import stdout
import threading
from datetime import timedelta, datetime, time, timezone
from time import sleep
from typing import Optional

from config import EMAIL_CLIENT
from email_client import send_email
from model import SolarPanelData, get_db

# from playhouse.shortcuts import model_to_dict

from http_client import MAX_ANGLE, DefaultAPIService
from model import SolarPosition, HistoricalData, ProductionData
from panel_controller import PanelController
from publisher import MQTTPublisher


DEFAULT_WAIT_TIME = timedelta(days=1).total_seconds()
DEFAULT_CALL_INTERVAL = timedelta(minutes=1).total_seconds()

MINIMUM_THRESHOLD = float(os.environ.get('MINIMUM_THRESHOLD', 100.0))
EFFICIENCY_DAYS_CHECK = int(os.environ.get('EFFICIENCY_DAYS_CHECK', 2))


class ConsumerException(Exception):
    def __init__(self, message):
        super().__init__(f"Consumer polling failed: {message}")


class ConsumerMaxRetriesError(ConsumerException):
    def __init__(self):
        super().__init__("Max retries")


class APIConsumer:
    def __init__(
            self,
            api_service: DefaultAPIService,
            publisher: MQTTPublisher,
            call_interval: Optional[int] = None,
    ):

        self.marked_to_sleep = False
        self.sleep_time = DEFAULT_WAIT_TIME
        self.call_interval = (
            call_interval if call_interval else DEFAULT_CALL_INTERVAL
        )
        self.api_service = api_service
        self.publisher = publisher

        self.publisher.connect()
        self.last_tilt: float | None = None
        self.last_position: str | None = None
        print('Connected to publisher', self.publisher.mqtt_client.__str__())

    def mark_to_sleep(self, seconds: float):
        self.marked_to_sleep = True
        self.sleep_time = seconds

    def _api_polling(self):
        if self.marked_to_sleep:
            sleep(self.sleep_time)

        print('Running infinite poll')

        while True:
            try:
                data = self.api_service.get_data()
                print('Sending data to solar', str(data))
                stdout.flush()
                db = next(get_db())
                lat_lon = self.api_service.get_lat_lon()
                if self.last_tilt is None:
                    self.last_tilt = MAX_ANGLE

                panel_controller = PanelController(
                    latitude=lat_lon['lat'],
                    longitude=lat_lon['lon'],
                    current_tilt=self.last_tilt,
                    max_angle=MAX_ANGLE,
                    last_position=self.last_position
                )

                position = panel_controller.get_default_position(self.api_service.get_now_time())

                with db.atomic():
                    solar_data: SolarPosition = SolarPosition.create(
                        timestamp=datetime.now().timestamp(),
                        azimuth=data['azimuth'],
                        altitude=90 - data['zenith'],
                        latitude=lat_lon['lat'],
                        longitude=lat_lon['lon']
                    )

                    solar_data.save()

                    # if self.last_tilt != panel_controller.position_to_tilt[position]:
                    print(f'UTC{self.api_service.get_now_time()} Sending rotate data - pos: ', position)
                    self.publisher.publish_to_rotate(
                        { "position": position }
                    )

                    self.last_tilt = panel_controller.position_to_tilt[position]
                    self.last_position = panel_controller.last_position 

                now_time = self.api_service.get_now_time()

                if now_time >= datetime(
                    year=now_time.year, 
                    month=now_time.month,
                    day=now_time.day,
                    hour=time(11).hour,
                    second=1,
                    minute=0,
                    tzinfo=timezone.utc
                ) and now_time <= datetime(
                    year=now_time.year, 
                    month=now_time.month,
                    day=now_time.day,
                    hour=time(11).hour,
                    second=1,
                    minute=5,
                    tzinfo=timezone.utc
                ):
                    self.check_efficiency()

                check, data = self.api_service.get_wheather()
                print('Sending wheather data to solar', f'{str(list(data.items())[:5])} ...')
                stdout.flush()

                if check is not False:
                    timestamp = datetime.strptime(data["time"], "%Y-%m-%dT%H:%M")
                    with db.atomic():
                        wheather_data: HistoricalData = HistoricalData.create(
                            Timestamp=timestamp,
                            Temperature_2m=data["temperature_2m"],
                            Relative_Humidity_2m=data["relative_humidity_2m"],
                            Apparent_Temperature=data["apparent_temperature"],
                            Is_Day=data["is_day"],
                            Precipitation=data["precipitation"],
                            Cloud_Cover=data["cloud_cover"]
                        )
                        wheather_data.save()

                check, data = self.api_service.get_production()
                if check is not False:
                    print(f'Sending production data to solar {str(list(data.items())[:5])} ... ')
                    stdout.flush()

                    with db.atomic():

                        def save_production_data(timestamp, radiation_value):
                            if all(map(lambda x: x is not None and x, (timestamp, radiation_value))) and radiation_value > 0.0:
                                production_data = ProductionData.create(
                                    DateTime=timestamp,
                                    Radiation=radiation_value
                                )
                                production_data.save()


                        for timestamp, radiation_value in data.items():
                            try:

                                existing_data = ProductionData.get(
                                    ProductionData.DateTime == timestamp)

                                if existing_data is not None:
                                    existing_data.Radiation = radiation_value
                                    existing_data.save()
                                else:
                                    save_production_data(timestamp, radiation_value)

                            except DoesNotExist:
                                save_production_data(timestamp, radiation_value)

                sleep(DEFAULT_CALL_INTERVAL)

            except Exception as e:
                print(f'Error in API polling: {e}')
                stdout.flush()
                sleep(DEFAULT_CALL_INTERVAL)

    def check_efficiency(self):
        print(f'Checking efficiency of the last {EFFICIENCY_DAYS_CHECK} days...')
        now = self.api_service.get_now_time()
        two_days_ago = now - timedelta(days=EFFICIENCY_DAYS_CHECK)
        db = next(get_db())

        with db.atomic():
            # Calculate the expected number of records
            # If logs are created every 30 seconds 
            # from 5am to 7pm, there would be 
            # 2 (logs per minute) * 60 (minutes per hour)
            # * 14 (hours per day) * 2 (days) = 40320 logs in two days.
            expected_records = (2 * 60 * 14 * 2) - 320

            # Create a query that selects data from the last 2 days
            query = (SolarPanelData
                     .select()
                     .where(
                         (SolarPanelData.timestamp.between(two_days_ago, now)) &
                         (fn.extract('hour', SolarPanelData.timestamp).between(5, 19))
                     ))

            # Count the number of records in the query
            record_count = query.count()

            # Only check the average potency if the number of records is equal to the expected number
            if record_count >= expected_records:
                # Create a query that calculates the average potency
                query = (SolarPanelData
                         .select(fn.AVG(SolarPanelData.potency).alias('average_potency'))
                         .where(
                             (SolarPanelData.timestamp.between(two_days_ago, now)) &
                             (fn.extract('hour', SolarPanelData.timestamp).between(5, 19))
                         ))

            # Execute the query and get the average potency
                average_potency = query.get().average_potency

                # Check if the average potency is less than the minimum threshold
                if average_potency < MINIMUM_THRESHOLD:
                    send_email(
                        to=EMAIL_CLIENT,
                        text=f"""
Detectamos uma queda de produção de energia abaixo da potência mínima cadastrada.

Verifique as condições do painel.

Att.: Suporte - Equipe Tracker.
            """
                )
            else:
                print(f'Insufficient records to check efficiency. Found: {record_count if record_count else 0}')


    def start_polling(self) -> threading.Thread:
        # task = loop.create_task(self._api_polling(), name="API Polling Task")

        print('Starting polling....')
        task = threading.Thread(target=self._api_polling)
        task.start()
        return task

import os
import mailtrap as mt


SEND_EMAIL_TOKEN = os.environ.get('EMAIL_SEND_TOKEN', "74efe3c74a738daedab8d4b4076e427d")
RETRIES=2

def send_email(
    to="adriansoareslps@gmail.com",
    subject="Tracker Girassol Problems",
    text=""
    ):
    mail = mt.Mail(
        sender=mt.Address(email="mailtrap@demomailtrap.com", name="Mailtrap Test"),
        to=[mt.Address(email=to)],
        subject=subject,
        text=text,
        category="Developer Test",
    )
    count = RETRIES 

    while count > 0:
        try:
            print(f'Sending email to {to}...')
            client = mt.MailtrapClient(token=SEND_EMAIL_TOKEN)
            client.send(mail)
            break
        except:
            print("Nao foi possivel enviar o email de falha...")
            if count > 0:
                count -= 1 
                continue

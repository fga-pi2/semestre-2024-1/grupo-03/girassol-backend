from sys import stdout
from time import sleep
from typing import Any

from email_client import send_email
from geocoder.base import requests
from paho.mqtt.client import Client, MQTTMessage
from paho.mqtt.enums import MQTTErrorCode
import geocoder
from requests import Response

from http_client import DefaultAPIService
from publisher import MQTTPublisher
from config import EMAIL_CLIENT, MQTT_SERVICE_NAME, MQTT_SERVICE_PORT
from model import HistoricalData, LatLong, LocalSolarData, ProductionData, PositionData, SolarPanelData, SolarPosition, get_db
import asyncio
import json
import os
from threading import Thread, Timer
from datetime import datetime as dt, timezone

from consumer import DEFAULT_WAIT_TIME, APIConsumer, ConsumerException, ConsumerMaxRetriesError


def create_tables():
    db = next(get_db())
    with db as database:
        database.create_tables([ProductionData, SolarPosition, LatLong, HistoricalData, LocalSolarData, SolarPanelData, PositionData])


class MQTTServerHandler:
    def __init__(self):
        self.mqtt_client = Client()
        # self.mqtt_client.on_message = self.command_callback
        self.mqtt_client.message_callback_add('production_data', self.production_data_callback)
        # self.mqtt_client.message_callback_add('position_data', self.position_data_callback)
        self.started = False

    def has_started(self):
        return self.started

    def connect(self):
        self.mqtt_client.connect(MQTT_SERVICE_NAME, MQTT_SERVICE_PORT, 60)

        self.mqtt_client.subscribe([("production_data", 1)])
        self.mqtt_client.subscribe([("position", 1)])

        self.mqtt_client.message_callback_add(
            "production_data", self.production_data_callback)

        self.mqtt_client.message_callback_add(
            "position", self.position_callback)

        self.timer = Timer(61, self.handle_timeout)  # Set timer for 60 seconds

    def position_callback(self, client: Client, userdata: Any, message: MQTTMessage):
        return 
        # Add data to database
        #try:
          #  print(f"Position received message: {message.payload.decode()}")
          #  data = json.loads(message.payload.decode())
          # db = next(get_db())
          #  with db.atomic():
          #      position_data = PositionData.create(
          #          timestamp=dt.now().timestamp(),
          #          angle=data[]['angle']
          #      )
          #      position_data.save()
        #except json.JSONDecodeError as e:
        #    print(e.__str__())
        #    print(f"Error decoding message: {e}")
        #    return
        #except Exception as e:
        #    print(e.__str__())
        #    print(f"Error processing message: {e}")
        #    return

    def production_data_callback(self, client: Client, userdata: Any, message: MQTTMessage):
        # Add data to database
        if self.timer.is_alive():
            self.timer.cancel()
        self.timer = Timer(61, self.handle_timeout)
        self.timer.start()

        try:
            print(f"Production data received message: {message.payload.decode()}")
            data = json.loads(message.payload.decode())
            db = next(get_db())
            with db.atomic():
                if data['data']['voltage'] >= 80.0:
                    production_data = SolarPanelData.create(
                        timestamp=dt.now(timezone.utc).timestamp(),
                        potency=data['data']['potency'],
                        voltage=data['data']['voltage'],
                        current=data['data']['current'],
                    )
                    production_data.save()
                else:
                    send_email(
                        subject="Tracker - Queda de tensão",
                        text="Painel solar sem tensão. Possivelmente está desligado. Por favor verifique o seu painel.",
                    )

        except json.JSONDecodeError as e:
            print(e.__str__())
            print(f"Error decoding message: {e}")
            return
        except Exception as e:
            print(e.__str__())
            print(f"Error processing message: {e}")
            return


    def send_position_comand(self, data):
        json_data = json.dumps({
            'position': data.position,
            'timestamp': dt.now(timezone.utc).timestamp()
        })
        self.mqtt_client.publish("position_data", json_data)

    def get_position(self):
        # This function should access database and define new position based
        # on the data
        return PositionCommand(position=1)

    def start(self) -> MQTTErrorCode:
        self.started = True
        self.timer.start()
        print('Timer: started with 60 seconds timeout for ping')
        return self.mqtt_client.loop_start()

    def stop(self):
        if self.mqtt_client.is_connected():
            self.mqtt_client.disconnect()

        self.started = False

    def handle_timeout(self):
        print(f"{dt.now(timezone.utc)}-Ping ERROR: Timeout reached")
        send_email(
            to=EMAIL_CLIENT,
            text="""
Não conseguimos receber dados do painel solar nos últimos 60 segundos.

Verifique o status do painel. Ou contate nosso suporte

Att.: Suporte - Equipe Tracker.
            """
        )


class PositionCommand:
    def __init__(self, position=None):
        self.position = position


def get_ip():
    response: Response | None = None
    counter = 0

    while counter < 5:
        try:
            response = requests.get('https://api64.ipify.org?format=json', timeout=5, allow_redirects=True)
            if response and response.status_code == 200 and response.json()['ip']:
                break
            else:
                raise Exception("Failed to get IP address")

        except Exception as e:
            print("Failed to IP address")
            print(e.__str__(), e)
            print("Retrying..............")
            counter+=1
            sleep(5)

            continue

    if response is None: 
        raise Exception("Failed to get IP address")

    print('Got IP: ', response.json()["ip"])
    stdout.flush()
    
    return response.json()["ip"]


def get_location():
    print('Getting IP and location.....')
    ip_address = get_ip()
    response: Response | None = None
    counter = 0

    while counter < 5:
        try:

            response = requests.get(
                f'https://ipapi.co/{ip_address}/json/', 
                timeout=10,
                allow_redirects=True
            )

            if response and response.status_code == 200 and response.json()['latitude']:
                break;

        except Exception as e:
            print("Failed to get location data")
            print(e.__str__(), e)
            print("Retrying..............")
            counter+=1
            stdout.flush()
            sleep(5)

            continue
        
    if response is None:
        raise Exception("Failed to get location data")

    data = response.json()

    location_data = {
        "lat": data.get("latitude"),
        "long": data.get("longitude"),
        "timestamp": dt.now(timezone.utc).timestamp()
    }

    stdout.flush()
    return location_data


def current_position() -> tuple[float, float]:
    lat, lng = float(os.environ.get("LAT", "0.0")), float(os.environ.get("LON", "0.0"))
    db = next(get_db())
    if lat and lng and (lat != 0.0 and lng != 0.0):
        with db.atomic():
            info = {
                "lat": lat,
                "long": lng,
                "timestamp": dt.now(timezone.utc).timestamp()
            }

            if LatLong.select().count() > 0:
                LatLong.delete().execute()

            LatLong(**info).save()

        return lat, lng

    info : dict[str, Any] = get_location()

    # if not info or not info.lat or not info.lng:
    #     raise Exception("Failed to get current position")
 
    with db.atomic():
        if LatLong.select().count() > 0:
            LatLong.delete().execute()
        LatLong(**info).save()

    return info['lat'], info['long']


def current_locale() -> str:
    locale = os.environ.get("LOCALE")
    if locale:
        return locale

    return geocoder.timezone('me')


async def main(handler: MQTTServerHandler):
    lat, lng = current_position()
    print('Got pos: ', lat, lng)
    # locale = current_locale()
    default_api_service = DefaultAPIService(lat=lat, lon=lng, locale='UTC')
    publisher = MQTTPublisher()
    apiConsumer = APIConsumer(api_service=default_api_service, publisher=publisher)
    handle_error: MQTTErrorCode | None = None
    res: Thread | None = None
    # while True:
        # loop = asyncio.new_event_loop()
    try:
        if not handler.has_started():
            handle_error = handler.start()

        consumer = apiConsumer.start_polling()

        if handle_error != MQTTErrorCode.MQTT_ERR_SUCCESS:
            raise Exception("MQTT listener failed to start.")

        print('Waiting polling...')
        stdout.flush()

        consumer.join()


    except ConsumerException as e:
        if isinstance(e, ConsumerMaxRetriesError):
            apiConsumer.mark_to_sleep(DEFAULT_WAIT_TIME)

        print(e.__str__())

        # continue

    except Exception as e:
        handler.stop()
        if res and res.is_alive():
            res.join(5.0)

        print(e.__str__())
        raise e

        # continue

if __name__ == "__main__":
    create_tables()
    handler = MQTTServerHandler()
    handler.connect()
    print(f'Connected to MQTT server: {handler.mqtt_client.is_connected()}')
    print('Starting main loop....')
    asyncio.run(main(handler))
    
    # while True:
    #     command = handler.get_position()
    #     handler.send_position_command(command)
    #     # Probably add a function to be called pedriodically to send position

    #     time.sleep(5)
    # # end test code

from peewee import DateField, DateTimeField, FloatField, IntegerField, Model, PostgresqlDatabase, TimeField, TimestampField
from playhouse.shortcuts import ThreadSafeDatabaseMetadata

import config


db = PostgresqlDatabase(
    database=config.POSTGRES_DB,
    user=config.POSTGRES_USER,
    password=config.POSTGRES_PASSWORD,
    host=config.POSTGRES_HOST,
    port=config.POSTGRES_PORT
)


def get_db():
    #print('Connecting to database...')
    if db.is_closed():
        db.connect()

    #print(f'Database connection status: {not db.is_closed()}')

    yield db
    if not db.is_closed():
        db.close()


class BaseModel(Model):
    class Meta:
        model_metadata_class = ThreadSafeDatabaseMetadata
        database = db


class PositionData(BaseModel):
    timestamp: TimestampField = TimestampField(primary_key=True)
    angle: FloatField = FloatField()

    class Meta:
        table_name = "panel_position"


class SolarPosition(BaseModel):
    timestamp: TimestampField = TimestampField(primary_key=True)
    azimuth: FloatField = FloatField()
    altitude: FloatField = FloatField()
    latitude: FloatField = FloatField()
    longitude:  FloatField = FloatField()

    class Meta:
        table_name = 'solar_position'


class SolarPanelData(BaseModel):
    timestamp: TimestampField = TimestampField(primary_key=True)
    potency: FloatField = FloatField() # Amount of energy produced in kilowatt-hours (kWh).
    voltage: FloatField = FloatField() # - Voltage output of the panel at the time of recording.
    current: FloatField = FloatField() #- Current output of the panel at the time of recording.

    class Meta:
        table_name = 'panel_data'


class LocalSolarData(BaseModel):
    DATE: DateField = DateField()
    TIME: TimeField = TimeField()
    Direcao_Vento = FloatField()
    Pir_GHI_SN9045 = FloatField()
    SR05_SN9045_GHI = FloatField()
    Temperatura_Ar = FloatField()
    Umidade_Ar = FloatField()
    Velocidade_Vento = FloatField()
    
    class Meta:
        table_name = config.HISTORICAL_DATA_TABLE 
        primary_key = False


class HistoricalData(BaseModel):
    Timestamp = TimestampField()
    Temperature_2m = FloatField()
    Relative_Humidity_2m = FloatField()
    Apparent_Temperature = FloatField()
    Is_Day = IntegerField()
    Precipitation = FloatField()
    Cloud_Cover = IntegerField()

    class Meta:
        table_name = "historical_table"
        primary_key = False


class ProductionData(BaseModel):
    DateTime = DateTimeField(primary_key=True)
    Radiation = FloatField()

    class Meta:
        table_name = "production_table"


class LatLong(BaseModel):
    lat: FloatField = FloatField()
    long: FloatField = FloatField()

    class Meta:
        table_name = 'LatLong'
        primary_key = False

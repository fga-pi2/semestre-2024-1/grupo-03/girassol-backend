from geocoder.cli import json
from paho.mqtt.client import Client

from config import MQTT_SERVICE_NAME, MQTT_SERVICE_PORT


class MQTTPublisher:

    def __init__(self):
        self.mqtt_client = Client()
        self.started = False

    def has_started(self):
        return self.started

    def connect(self):
        self.mqtt_client.connect(MQTT_SERVICE_NAME, MQTT_SERVICE_PORT, 60)
        self.solar_path = "solar_data"
        self.rotate_path = "rotate"


    def publish_comand(self, queue: str, data: dict):
        json_data = json.dumps(data)
        self.mqtt_client.publish(queue, json_data)


    def publish_to_solar(self, data: dict):
        self.publish_comand(self.solar_path, data)


    def publish_to_rotate(self, data: dict):
        self.publish_comand(self.rotate_path, data)


from flask import Flask, render_template, request, jsonify
import json
import pandas as pd

# Criação da instância do Flask
app = Flask(__name__)

# Carregar dados do arquivo JSON
try:
    with open('dados.json', 'r') as json_file:
        dados = json.load(json_file)
except FileNotFoundError:
    dados = {}

# Definir endpoints
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/dados-gerais', methods=['GET'])
def get_dados_gerais():
    return jsonify(dados.get('dados_gerais', {}))

@app.route('/dados-sistema', methods=['GET'])
def get_dados_do_sistema():
    return jsonify(dados.get('dados_do_sistema', {}))

@app.route('/dados-geracao', methods=['GET'])
def get_dados_de_geracao():
    return jsonify(dados.get('dados_de_geracao', {}))

@app.route('/dados-performance', methods=['GET'])
def get_dados_de_performance():
    return jsonify(dados.get('dados_de_performance', {}))

@app.route('/dados-adicionais', methods=['GET'])
def get_dados_adicionais():
    return jsonify(dados.get('dados_adicionais', {}))

@app.route('/salvar-coordenadas', methods=['POST'])
def salvar_coordenadas():
    data = request.get_json()
    latitude = data.get('latitude')
    longitude = data.get('longitude')

    if latitude is not None and longitude is not None:
        print(f'Latitude: {latitude}, Longitude: {longitude}')
        return jsonify({'message': 'Coordenadas recebidas e salvas com sucesso!'}), 200
    else:
        return jsonify({'error': 'Dados de coordenadas ausentes ou inválidos'}), 400

# Rota para retornar os dados da coluna Temperatura_Ar como JSON
@app.route('/dados-temperatura', methods=['GET'])
def get_temperatura_xlsx():
    try:
        df = pd.read_excel('Janeiro_1.xlsx', engine='openpyxl')
        
        colunas_selecionadas = [df.columns[0],df.columns[1]]
        df_selecionado = df[colunas_selecionadas]
        
        dados = df_selecionado.to_dict(orient='records')
        
        return jsonify({'dados': dados}), 200
    except FileNotFoundError:
        return jsonify({'error': 'Arquivo XLSX não encontrado'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 500

# Rota para retornar os dados da coluna Pir_GHI_SN9045 como JSON
@app.route('/dados-irradiacao-global', methods=['GET'])
def get_irradiacao_xlsx():
    try:
        df = pd.read_excel('Janeiro_1.xlsx', engine='openpyxl')
        
        colunas_selecionadas = [df.columns[1],df.columns[0],df.columns[6]]
        df_selecionado = df[colunas_selecionadas]
        
        dados = df_selecionado.to_dict(orient='records')
        
        return jsonify({'dados': dados}), 200
    except FileNotFoundError:
        return jsonify({'error': 'Arquivo XLSX não encontrado'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 500
    
# Rodar a aplicação
if __name__ == '__main__':
    app.run(debug=True)

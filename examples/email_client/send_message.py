import smtplib
import config
from email.message import Message


def send_email(destination: str, payload: str):

    msg = Message()
    msg['From'] = config.SMTP_FROM
    msg['To'] = destination

    msg.set_type('text/html')
    msg.set_charset("utf-8")
    msg.set_payload(payload)

    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as server:
        server.login(config.SMTP_FROM, config.SMTP_PASSWORD)
        server.send_message(msg)

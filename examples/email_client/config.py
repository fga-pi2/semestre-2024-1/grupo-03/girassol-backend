import os


SMTP_FROM = os.getenv('SMTP_FROM', '')
SMTP_PASSWORD = os.getenv('SMTP_PASSWORD', '')

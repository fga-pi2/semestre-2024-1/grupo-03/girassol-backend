#include <AccelStepper.h>

/* Defina os pinos que serão utilizados */
AccelStepper stepper(1, 12, 11); // EN=1 ENABLE, CW=11 DIRECAO, CLK=12 PULSO



void setup() {
  stepper.setMaxSpeed(50); // Define a velocidade máxima permitida
  stepper.setAcceleration(100); // Define a taxa de aceleração do motor
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);

  // Define a posição inicial do motor
  stepper.setCurrentPosition(0);
  
  stepper.enableOutputs(); // Habilita as saídas do driver do motor
}

void loop() {




  unsigned long startTime = millis(); // Tempo inicial

  // Mover para a posição 700 (sentido horário)
  while (stepper.distanceToGo() != 0) { // Enquanto houver distância a percorrer
    stepper.run(); // Aciona o motor
  }
  
  // Parar suavemente definindo a velocidade de destino como zero
  stepper.moveTo(stepper.currentPosition());
  while (stepper.distanceToGo() != 0) { // Enquanto houver distância a percorrer
    stepper.run();
  }
  
  delay(1000); // Aguarda 1 segundo

  startTime = millis(); // Atualiza o tempo inicial

  // Mover para a posição -700 (sentido anti-horário)
  stepper.moveTo(-300);
  while (stepper.distanceToGo() != 0) { // Enquanto houver distância a percorrer
    stepper.run(); // Aciona o motor
  }

  // Parar suavemente definindo a velocidade de destino como zero
  stepper.moveTo(stepper.currentPosition());
  while (stepper.distanceToGo() != 0) { // Enquanto houver distância a percorrer
    stepper.run();
  }
}
  

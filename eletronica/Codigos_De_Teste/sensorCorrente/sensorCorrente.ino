#include "EmonLib.h"                   // Inclui a biblioteca EmonLib
EnergyMonitor emon1;                 // Cria um objeto chamado emon01
float leitura = A0;                    
void setup()
{  
    Serial.begin(115200);                     // Inicia a serial com uma taxa de 115200
    emon1.current(leitura, 91);           // Corrente: pino de leitura, calibração.
}
void loop()
{
      double Irms = emon1.calcIrms(1480);  //Calcula a corrente RMS com 1480 amostras
        Serial.print(Irms);             // Irms
      Serial.println("A");
}
#include <AccelStepper.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>


// Definições dos pinos
#define GIR1 D1
#define GIR2 D2
#define DIR_PIN D7        // Pino de direção (DIR)
#define PUL_PIN D8        // Pino de pulso (STEP)

#define LIMIT_SWITCH1 D5  // Fim de curso 1
#define LIMIT_SWITCH2 D6  // Fim de curso 2
int countR = 0;


// Criação do objeto AccelStepper
AccelStepper stepper(1, PUL_PIN, DIR_PIN);

// Configurações da rede Wi-Fi
const char* ssid = "HR4";
const char* password = "@Senha12345678";

// Configurações do servidor MQTT
const char* mqtt_server = "192.168.71.220";
const int mqtt_port = 1883;
const char* mqtt_user = "admin";
const char* mqtt_password = "$7$101$xuOfObQdv0Hi2p0A$o+J3vzbpm0hekukYw73tT3fiB2Ogi1UCJgbzNyFA0GOgxAo79hqfTeXVr062KoD5nCphwV+1V/NxlMzwnV5Kvg==";

// Cliente WiFi e MQTT
WiFiClient espClient;
PubSubClient client(espClient);


void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conectado");
  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop até que esteja conectado
  while (!client.connected()) {
    Serial.print("Tentando conexão MQTT...");
    // Tenta se conectar ao servidor MQTT
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      Serial.println("conectado");
      client.subscribe("rotate");
    } else {
      Serial.print("falhou, rc=");
      Serial.print(client.state());
      Serial.println(" tenta novamente em 5 segundos");
      // Espera 5 segundos antes de tentar novamente
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  String message;
  for (unsigned int i = 0; i < length; i++) {
    message += (char)payload[i];
  }

  Serial.print("Mensagem recebida [");
  Serial.print(topic);
  Serial.print("]: ");
  Serial.println(message);

  // Crie um buffer estático de memória para analisar a mensagem JSON
  StaticJsonDocument<200> doc;

  // Analisando a mensagem JSON
  DeserializationError error = deserializeJson(doc, message);

  // Verifique se houve erro na análise
  if (error) {
    Serial.print("Falha na análise do JSON: ");
    Serial.println(error.c_str());
    return;
  }

  // Assumindo que o JSON contém um campo "command"
  const char* position = doc["position"];

  if (strcmp(position, "pos1") == 0) {
    movePos1();
  } else if (strcmp(position, "pos2") == 0) {
    movePos2();
  } else if (strcmp(position, "pos3") == 0) {
    movePos3();
  } else {
    Serial.println("Comando não reconhecido.");
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  ESP.wdtDisable();

  // Configuração dos pinos de fim de curso
  pinMode(LIMIT_SWITCH1, INPUT_PULLUP);
  pinMode(LIMIT_SWITCH2, INPUT_PULLUP);

  
  // Configuração do motor de passo
  stepper.setMaxSpeed(500);  // Velocidade máxima em passos por segundo
  stepper.setAcceleration(300);  // Aceleração em passos por segundo^2
  //stepper.moveTo(10000);

}

void loop() {
    ESP.wdtFeed();
  if (!countR){
    moveInicio();
  }
  
  //if(digitalRead(LIMIT_SWITCH1) == 0){
  //  stepper.moveTo(10000);

  //}
  //else if(digitalRead(LIMIT_SWITCH2) == 0){
  //  stepper.moveTo(-10000);
  //}

  //stepper.run();

  
}


int stepCounter = 0; // Variável de contagem de passos

void moveInicio() {
  stepper.moveTo(-15000);

  while (digitalRead(LIMIT_SWITCH1) != LOW) {
    ESP.wdtFeed();
    stepper.run();
  }

    
  stepper.setCurrentPosition(0); // Define a posição atual como zero
  countR++;
  movePos1();

  //calculaCurso();
}

void movePos1(){
  stepper.moveTo(700);
  while (stepper.distanceToGo() != 0) {
    stepper.run();
    ESP.wdtFeed();
  }
  delay(3000);
  movePos2();
  
}

void movePos2(){
  stepper.moveTo(2000);
  while (stepper.distanceToGo() != 0) {
    stepper.run();
    ESP.wdtFeed();
  }
  delay(3000);
  movePos3();
  
}

void movePos3(){
  stepper.moveTo(3500);
  while (stepper.distanceToGo() != 0 ) {
    stepper.run();
    ESP.wdtFeed();
  }

  
}


void calculaCurso(){
  stepper.moveTo(10000);
  while(digitalRead(LIMIT_SWITCH2) != LOW){
    ESP.wdtFeed();
    stepper.run();
  }

  
    
  //Serial.print("faltam ");
  Serial.print(stepper.distanceToGo());
  //Serial.println(" para 10000");
}
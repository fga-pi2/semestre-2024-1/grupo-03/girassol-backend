#include <ADS1115_WE.h>
#include <Wire.h>
#include "EmonLib.h"                   // Inclui a biblioteca EmonLib
EnergyMonitor emon1;                 // Cria um objeto chamado emon01
float leitura = A0; 

// Endereço do módulo ADS1115
#define I2C_ADDRESS 0x48

// Variáveis sensor tensão
int tensaoInst[300];
float tensao_rms;
float tensao_pico;
double maiorValorTensao = 0;
double tensao_valor = 0;

ADS1115_WE adc(I2C_ADDRESS);

void setup() 
{
  Wire.begin();
  emon1.current(leitura, 91);
  Serial.begin(9600);
  
  if (!adc.init())
  {
    Serial.println("ADS1115 não conectado!");
    while (1);
  }
  
  adc.setVoltageRange_mV(ADS1115_RANGE_6144); // Range configurado para ±6144 mV
  adc.setMeasureMode(ADS1115_CONTINUOUS);

  Serial.println("ADS1115 com ESP8266 NodeMCU - Leitura contínua");
  Serial.println("Todos os valores em escala 0-1023!");
  Serial.println();
}

void loop() 
{


}

int readChannelAsAnalog(ADS1115_MUX channel) 
{
  adc.setCompareChannels(channel);
  float voltage = adc.getResult_mV();
  return map(voltage, 0, 5000, 0, 1023); // Converte milivolts para uma escala de 0 a 1023
}

float leSensorTensao(){
maiorValorTensao = 0;
  for(int i = 0; i < 300 ; i++){
  tensaoInst[i] = readChannelAsAnalog(ADS1115_COMP_0_GND);   
  }
  
  //Encontrar o maior valor
  for(int i = 0; i < 300 ; i++){
    if(maiorValorTensao < tensaoInst[i]){
    maiorValorTensao = tensaoInst[i];
    }
  }
  //Serial.print("Maior Valor:");
  //Serial.println(maiorValorTensao);
  delay(1000);

   
  /*Valor de PICO ANALógic = 461
   * Valor lido no multímetro = 222 Vrms ----- 
   * Valor de PICO REAL = 222*1,4 = 310,8V.
   *ZERO analógico = 270, ou seja, aproximadamente = (270/1024)*5 = 1,31 V 
  * Temos que a variação do Zero ao PICO foi em uma relação de:  No Arduino - de 1,31V a 2,25 V [valor de pico, pois (461/1023)*5 = 2,25 V ]
  * Já na rede elétrica a variação foi de 0 a 310,8V.
  * Temos a seguinte relação = 270 a 461 ------------- 0 a 310,8
   */
  tensao_pico = map(maiorValorTensao,273,423,0,313);

  tensao_rms = tensao_pico/1.4;

  Serial.print("Tensão da Rede Elétrica: ");
  Serial.println(tensao_rms);

  return tensao_rms;

}

double leCorrente(){
  double Irms = emon1.calcIrms(1480);  //Calcula a corrente RMS com 1480 amostras
        Serial.print(Irms);             // Irms
      Serial.println("A");

      return Irms;
}

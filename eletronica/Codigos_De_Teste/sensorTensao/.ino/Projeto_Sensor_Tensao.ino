int corrente_inst[300];
int zero= 0;
int diferenca = 0;
int leitura = 0;
int pino_sensor = A0;
float corrente_pico;
float corrente_eficaz;

float tensao_rms;
float tensao_pico;

double maior_valor=0;
double corrente_valor=0;
unsigned long tempo_zero_inicio;
unsigned long tempo_zero_final;
unsigned long semi_periodo;
float frequencia_sinal;
unsigned long periodo_completo;
void setup() {
  Serial.begin(9600);
  pinMode(pino_sensor,INPUT);
}

void loop() {
  maior_valor = 0;
  for(int i = 0; i < 300 ; i++){
  corrente_inst[i] = analogRead(pino_sensor);   
  }
  
  //Encontrar o maior valor
  for(int i = 0; i < 300 ; i++){
    if(maior_valor < corrente_inst[i]){
    maior_valor = corrente_inst[i];
    }
  }
  Serial.print("Maior Valor:");
  Serial.println(maior_valor);
  delay(5000);

   
  /*Valor de PICO ANALógic = 461
   * Valor lido no multímetro = 222 Vrms ----- 
   * Valor de PICO REAL = 222*1,4 = 310,8V.
   *ZERO analógico = 270, ou seja, aproximadamente = (270/1024)*5 = 1,31 V 
  * Temos que a variação do Zero ao PICO foi em uma relação de:  No Arduino - de 1,31V a 2,25 V [valor de pico, pois (461/1023)*5 = 2,25 V ]
  * Já na rede elétrica a variação foi de 0 a 310,8V.
  * Temos a seguinte relação = 270 a 461 ------------- 0 a 310,8
   */
  tensao_pico = map(maior_valor,270,470,0,313);

  tensao_rms = tensao_pico/1.4;

  Serial.print("Tensão da Rede Elétrica: ");
  Serial.println(tensao_rms);

}

//#########################

// Código Esp 8266 - NODEMCU 1.0 Grupo Tracker Solar PI - 2 2024/1

// Código elaborado por Hugo Rocha, Jorge Guilherme e Daniel Alves, com inclusão de bibliotecas de diversos autores.

//#########################



// INCLUSÃO DE BIBLIOTECAS

#include <AccelStepper.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// DEFINIÇÃO DO ENDEREÇO I2C DO MPU6050
#define MPU 0x68

// Definições dos pinos
#define GIR1 D1
#define GIR2 D2
#define DIR_PIN D7        // Pino de direção (DIR)
#define PUL_PIN D8        // Pino de pulso (STEP)
#define LIMIT_SWITCH1 D5  // Fim de curso 1
#define LIMIT_SWITCH2 D6  // Fim de curso 2


 


////////////////////////////////////////////////////////
////////VARIAVEIS GLOBAIS

//Angulos
float Acc[2];
float Gy[3];
float Angle[3];
float angulo = 0;

String valores;

long tiempo_prev;
float dt;

//RANGE DE CONVERSAO DOS ANGULOS
#define A_R 16384.0 // 32768/2
#define G_R 131.0 // 32768/250
 
//CONVERSAO DE RADIANOS PARA GRAUS 180/PI
#define RAD_A_DEG = 57.295779
 
//MPU-6050 VALORES INTEIROS EM 16BITS
int16_t AcX, AcY, AcZ, GyX, GyY, GyZ;

// POSIÇÃO ATUAL
int POSITION;

//////////////////////////////////////////////////////


// Criação do objeto AccelStepper
AccelStepper stepper(1, PUL_PIN, DIR_PIN);

// Configurações da rede Wi-Fi
const char* ssid = "TrackerSolarSS";
const char* password = "@Trackerpi22024";

// Configurações do servidor MQTT
const char* mqtt_server = "192.168.1.2";
const int mqtt_port = 1883;
const char* mqtt_user = "";
const char* mqtt_password = "";

// Cliente WiFi e MQTT
WiFiClient espClient;
PubSubClient client(espClient);

// FUNÇÃO PADRAO PARA CONFIGURAÇÃO DO WIFI
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conectado");
  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP());
}

// FUNÇÃO DE RECONEXÃO EM CASO DE FALHA
void reconnect() {
  // Loop até que esteja conectado
  while (!client.connected()) {
    ESP.wdtFeed();
    Serial.print("Tentando conexão MQTT...");
    // Tenta se conectar ao servidor MQTT
    if (client.connect("placaMotor", mqtt_user, mqtt_password)) {
      Serial.println("conectado");
      client.subscribe("rotate");
    } else {
      Serial.print("falhou, rc=");
      Serial.print(client.state());
      Serial.println(" tenta novamente em 5 segundos");
      // Espera 5 segundos antes de tentar novamente
      delay(5000);
    }
  }
}

// MOVE A PLACA PARA A PRIMEIRA POSIÇÃO (ATÉ BATER NO FIM DE CURSO)
void movePos1() {
  POSITION = 1;
  stepper.moveTo(-15000);

  while (digitalRead(LIMIT_SWITCH1) != LOW) {
    ESP.wdtFeed();
    stepper.run();
  }
  angulo = 25.6;  
  stepper.setCurrentPosition(0); // Define a posição atual como zero
}


// MOVE A PLACA PARA A SEGUNDA POSIÇÃO

void movePos2(){
  if(POSITION == 1){
    stepper.moveTo(2000);
  } else {
    stepper.moveTo(-2000);
  }
  
  while (stepper.distanceToGo() !=0) {
    stepper.run();
    ESP.wdtFeed();
  }
  stepper.setCurrentPosition(0); // Define a posição atual como zero
  angulo = 0.15;
  POSITION = 2;
}

// MOVE A PLACA PARA A TERCEIRA POSIÇÃO (ATÉ BATER NO FIM DE CURSO)

void movePos3() {
  POSITION = 3;
  stepper.moveTo(15000);

  while (digitalRead(LIMIT_SWITCH2) != LOW) {
    ESP.wdtFeed();
    stepper.run();
  }
  angulo = 22.30;  
  stepper.setCurrentPosition(0); // Define a posição atual como zero
}

//FUNÇÃO QUE RECEBE O CALLBACK DO MQTT
void callback(char* topic, byte* payload, unsigned int length) {
  String message;
  for (unsigned int i = 0; i < length; i++) {
    message += (char)payload[i];
  }

  Serial.print("Mensagem recebida [");
  Serial.print(topic);
  Serial.print("]: ");
  Serial.println(message);

  // Crie um buffer estático de memória para analisar a mensagem JSON
  StaticJsonDocument<200> doc;

  // Analisando a mensagem JSON
  DeserializationError error = deserializeJson(doc, message);

  // Verifique se houve erro na análise
  if (error) {
    Serial.print("Falha na análise do JSON: ");
    Serial.println(error.c_str());
    return;
  }

  // Assumindo que o JSON contém um campo "command"
  const char* position = doc["position"];

  if (strcmp(position, "pos1") == 0) {
    movePos1();
  } else if (strcmp(position, "pos2") == 0 && POSITION != 2) {
    movePos2();
  } else if (strcmp(position, "pos3") == 0) {
    movePos3();
  } 
  else {
    Serial.println("Comando não reconhecido.");
  }

  String jsonString = "{\"data\":{";
  jsonString += "\"angle\":"; 
  jsonString += angulo;
  jsonString += "}}";

  client.publish("position", jsonString.c_str());
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  ESP.wdtDisable();

Wire.begin(4,5); // D2(GPIO4)=SDA / D1(GPIO5)=SCL
Wire.beginTransmission(MPU);
Wire.write(0x6B);
Wire.write(0);
Wire.endTransmission(true);

  // Configuração dos pinos de fim de curso
  pinMode(LIMIT_SWITCH1, INPUT_PULLUP);
  pinMode(LIMIT_SWITCH2, INPUT_PULLUP);

  
  // Configuração do motor de passo
  stepper.setMaxSpeed(500);  // Velocidade máxima em passos por segundo
  stepper.setAcceleration(300);  // Aceleração em passos por segundo^2
  //stepper.moveTo(10000);
  movePos1();

  angulo = 25.6;
  String jsonString = "{\"data\":{";
  jsonString += "\"angle\":"; 
  jsonString += angulo;
  jsonString += "}}";

  // Publica o JSON no tópico 'sensores/multi'
  client.publish("position", jsonString.c_str());
  

}

void loop() {
  ESP.wdtFeed();
  if(!client.connected()){
    reconnect();
  }
  client.loop();



int stepCounter = 0; // Variável de contagem de passos

float leAngulo(){
  Wire.beginTransmission(MPU);
   Wire.write(0x3B); 
   Wire.endTransmission(false);
   Wire.requestFrom(MPU,6,true);   
   AcX=Wire.read()<<8|Wire.read(); 
   AcY=Wire.read()<<8|Wire.read();
   AcZ=Wire.read()<<8|Wire.read();
  ESP.wdtFeed();
   Acc[1] = atan(-1*(AcX/A_R)/sqrt(pow((AcY/A_R),2) + pow((AcZ/A_R),2)))*RAD_TO_DEG;
   Acc[0] = atan((AcY/A_R)/sqrt(pow((AcX/A_R),2) + pow((AcZ/A_R),2)))*RAD_TO_DEG;
 
  
   Wire.beginTransmission(MPU);
   Wire.write(0x43);
   Wire.endTransmission(false);
   Wire.requestFrom(MPU,6,true);   
   GyX=Wire.read()<<8|Wire.read(); 
   GyY=Wire.read()<<8|Wire.read();
   GyZ=Wire.read()<<8|Wire.read();
 
  
   Gy[0] = GyX/G_R;
   Gy[1] = GyY/G_R;
   Gy[2] = GyZ/G_R;

   dt = (millis() - tiempo_prev) / 1000.0;
   tiempo_prev = millis();
 
   Angle[0] = 0.98 *(Angle[0]+Gy[0]*dt) + 0.02*Acc[0];
   Angle[1] = 0.98 *(Angle[1]+Gy[1]*dt) + 0.02*Acc[1];


   Angle[2] = Angle[2]+Gy[2]*dt;

  return (1.0530*Angle[0]+3.5873);

  
  
  Serial.println(1.0530*Angle[0]+3.5873);
  
   delay(10);
}


void calculaCurso(){
  stepper.moveTo(10000);
  while(digitalRead(LIMIT_SWITCH2) != LOW){
    ESP.wdtFeed();
    stepper.run();
  }
    
  
  Serial.print(stepper.distanceToGo());
}
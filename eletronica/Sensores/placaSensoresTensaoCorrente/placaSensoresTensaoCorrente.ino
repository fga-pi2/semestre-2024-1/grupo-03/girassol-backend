#include <ADS1115_WE.h>
#include <Wire.h>
#include "EmonLib.h"   
#include <ESP8266WiFi.h>
#include <PubSubClient.h>


EnergyMonitor emon1;                 // Cria um objeto chamado emon01
float leitura = A0;


// Endereço do módulo ADS1115
#define I2C_ADDRESS 0x48

// Variáveis sensor tensão
int tensaoInst[300];

unsigned long tempoInicial;


ADS1115_WE adc(I2C_ADDRESS);

// Configurações da rede Wi-Fi
const char* ssid = "TrackerSolarSS";
const char* password = "@Trackerpi22024";

// Configurações do servidor MQTT
const char* mqtt_server = "192.168.1.2";
const int mqtt_port = 1883;
const char* mqtt_user = "";
const char* mqtt_password = "";

// Cliente WiFi e MQTT
WiFiClient espClient;
PubSubClient client(espClient);


void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conectado");
  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop até que esteja conectado
  while (!client.connected()) {
    ESP.wdtFeed();
    Serial.print("Tentando conexão MQTT...");
    // Tenta se conectar ao servidor MQTT
    if (client.connect("placaSensores", mqtt_user, mqtt_password)) {
      Serial.println("conectado");
    } else {
      Serial.print("falhou, rc=");
      Serial.print(client.state());
      Serial.println(" tenta novamente em 5 segundos");
      // Espera 5 segundos antes de tentar novamente
      ESP.wdtFeed();
      delay(500);
    }
  }
}

int readChannelAsAnalog(ADS1115_MUX channel) 
{
  adc.setCompareChannels(channel);
  float voltage = adc.getResult_mV();
  return map(voltage, 0, 5000, 0, 1023); // Converte milivolts para uma escala de 0 a 1023
}

float leSensorTensao(){
  double maiorValorTensao = 0;
  for(int i = 0; i < 300 ; i++){
  tensaoInst[i] = readChannelAsAnalog(ADS1115_COMP_0_GND);
  ESP.wdtFeed();   
  }
  ESP.wdtFeed();
  
  //Encontrar o maior valor
  for(int i = 0; i < 300 ; i++){
    if(maiorValorTensao < tensaoInst[i]){
    maiorValorTensao = tensaoInst[i];
    ESP.wdtFeed();
    }
  }
  ESP.wdtFeed();
  //Serial.print("Maior Valor:");
  //Serial.println(maiorValorTensao);

   
  /*Valor de PICO ANALógic = 461
   * Valor lido no multímetro = 222 Vrms ----- 
   * Valor de PICO REAL = 222*1,4 = 310,8V.
   *ZERO analógico = 270, ou seja, aproximadamente = (270/1024)*5 = 1,31 V 
  * Temos que a variação do Zero ao PICO foi em uma relação de:  No Arduino - de 1,31V a 2,25 V [valor de pico, pois (461/1023)*5 = 2,25 V ]
  * Já na rede elétrica a variação foi de 0 a 310,8V.
  * Temos a seguinte relação = 270 a 461 ------------- 0 a 310,8
   */
  double tensao_pico = map(maiorValorTensao,273,423,0,313);

 float  tensao_rms = tensao_pico/1.4;


 // Serial.print("Tensão da Rede Elétrica: ");
  //Serial.println(tensao_rms);

  return tensao_rms;

}

double leSensorCorrente() {
  int contador =0;
  double corrente = emon1.calcIrms(1480);
  while(contador < 5){
    double Irms = emon1.calcIrms(1480);
    ESP.wdtFeed();
    if(Irms < corrente){
      corrente =Irms; 
    }
    contador++;
  }
  return corrente;
  
}

void setup() 
{
  tempoInicial = millis();
  Wire.begin();
  emon1.current(leitura, 95);
  Serial.begin(115200);
   ESP.wdtDisable();

  
  if (!adc.init())
  {
    Serial.println("ADS1115 não conectado!");
    while (1);
  }
  
  adc.setVoltageRange_mV(ADS1115_RANGE_6144); // Range configurado para ±6144 mV
  adc.setMeasureMode(ADS1115_CONTINUOUS);

  Serial.println("ADS1115 com ESP8266 NodeMCU - Leitura contínua");
  Serial.println("Todos os valores em escala 0-1023!");
  Serial.println();
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  
  int contador = 0;
  while(contador < 5) {
    leSensorCorrente(); 
    delay(1000);
    ESP.wdtFeed();
    leSensorTensao();  
    contador++;
  }
}



void enviaLeituras(){
  ESP.wdtFeed();

  // Aqui você deve coletar as leituras dos sensores
  double corrente = leSensorCorrente(); 
  Serial.println(corrente);
  //delay(1000);
  ESP.wdtFeed();
  
  float tensao = leSensorTensao();   

  //if(tensao < 80)
    //corrente = 0;
  //float tensao = 0.0; 
  double potencia = tensao * corrente;

  // Criação do JSON com múltiplos sensores
  String jsonString = "{\"data\":{";
  jsonString += "\"voltage\":" + String(tensao) + ",";
  jsonString += "\"current\":" + String(corrente) + ",";
  jsonString += "\"potency\":" + String(potencia);
  jsonString += "}}";

  // Publica o JSON no tópico 'sensores/multi'
  //if (millis() - tempoInicial >= 5000) {
    client.publish("production_data", jsonString.c_str());
    // Espera 10 segundos antes de enviar novamente
  //}
  
}



void loop() 
{
  if (!client.connected()) {
    reconnect();
    ESP.wdtFeed();
  }
  client.loop();

enviaLeituras();

}



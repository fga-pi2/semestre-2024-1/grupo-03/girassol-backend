# Girassol Backend

## Dados enviados pelo microcontrolador

### Produção

```json
{
    "event": Literal["Production"],
    "voltage": float,
    "current": float
}
```

### Climático


```json
{
    "event": "Temperature",
    "celsius": float
}
```

### Posição

```json
{
    "event": "Gyroscope",
    "angle": float
}
```

### Incidência solar


```json
{
    "event": "Irradiation",
    "ldr1": float,
    "ldr2": float
}
```

## Dados enviados pelo servidor 

### API Externa

```json
{
    "event": "ExternalData",
    "data": {
        "ghi": float,
        "dhi": float,
        "pos": { "lat": float, "lng": float },
        "azimuth": float,
        "zenith": float,
        "timestamp": int
    }
}
```

```json
{
    "event": "HistoricalData",
    "data": {
        "ghi": float,
        "dhi": float,
        "pos": { "lat": float, "lng": float },
        "azimuth": float,
        "zenith": float,
        "timestamp": int
    }
}
```

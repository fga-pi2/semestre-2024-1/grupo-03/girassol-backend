#!/usr/bin/bash

cd /home/girassol/girassol-backend/server

docker compose up -d db
sleep 5

source ../../rtl8812au/.venv/bin/activate
LAT="-15.9973" LON="-48.0509" MQTT_SERVICE_NAME="localhost" POSTGRES_HOST="localhost" python main.py
